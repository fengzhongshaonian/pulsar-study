package com.example;

import org.apache.pulsar.client.api.*;
import org.junit.Before;
import org.junit.Test;

/**
 * 测试Key_Shared模式
 * <pre>
 * 测试目的：
 * 1.对于两个订阅模式是Key_Shared的具有相同订阅名称的消费者，它们能否同时订阅同一个主题？结论：能。但是，具有相同key的消息只会被其中一个消费者消费。
 * 比如：消费者A消费了key为"key1"的消息，那么后续所以key为"key1"的消息都会由该消费者消费，而另一个消费者则消费不到key为"key1"的消息。
 *
 * 2.对于两个订阅模式是Key_Shared的具有不同订阅名称的消费者，它们能否同时订阅同一个主题？结论：能。而且，生产者往该主题发送的一条消息，这两个消费者都会消费到。
 * </pre>
 *
 * @author linfeng
 * @date 2022/2/25 14:44
 */
public class KeySharedSubscriptionTest {

    private PulsarClient pulsarClient;

    @Before
    public void init() throws PulsarClientException {
        pulsarClient = PulsarClient.builder()
                .serviceUrl("pulsar://localhost:6650")
                .build();
    }

    private Consumer<String> subscribe(String topicName, String subscriptionName) throws PulsarClientException {
        ConsumerBuilder<String> stringConsumerBuilder = pulsarClient.newConsumer(Schema.STRING)
                .topic(topicName)
                .subscriptionName(subscriptionName)
                .subscriptionMode(SubscriptionMode.Durable)
                .subscriptionType(SubscriptionType.Key_Shared);

        Consumer<String> consumer = stringConsumerBuilder.subscribe();

        System.out.println("成功订阅！订阅名：" + subscriptionName + ", 主题：" + topicName);

        return consumer;
    }

    @Test
    public void testKeySharedSubscription() throws PulsarClientException {

        Consumer<String> consumer = subscribe(TestProducer.TOPIC_NAME, "shared-subscription-one");

        while (true){
            Message<String> message = consumer.receive();
            System.out.println("收到消息：" + message.getValue() + ", 消息id:" + message.getMessageId() + ", key:" + message.getKey());
            consumer.acknowledge(message);
        }
    }

    @Test
    public void testKeySharedSubscription2() throws PulsarClientException {

        Consumer<String> consumer = subscribe(TestProducer.TOPIC_NAME, "shared-subscription-two");

        while (true){
            Message<String> message = consumer.receive();
            System.out.println("收到消息：" + message.getValue() + ", 消息id:" + message.getMessageId() + ", key:" + message.getKey());
            consumer.acknowledge(message);
        }
    }
}
