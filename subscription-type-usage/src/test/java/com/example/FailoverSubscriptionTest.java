package com.example;

import org.apache.pulsar.client.api.*;
import org.junit.Before;
import org.junit.Test;

/**
 * 灾备模式测试
 * <pre>
 * 测试目的：
 * 1.Failover订阅模式的两个具有相同订阅名的消费者能否同时订阅同一主题？结论：能。但是只有先连上broker的消费者能消费消息，后连上broker的消费者消费不到消息。
 * 只有前一个消费者挂了，后一个消费者才能顶替前一个消费者去消费尚未消费的以及新的消息。
 *
 * 2.Failover订阅模式的两个具有不同订阅名的消费者能否同时订阅同一主题？结论：能。而且生产者发送的消息，这两个消费者可以同时消费到。
 * </pre>
 *
 * @author linfeng
 * @date 2022/2/25 15:00
 */
public class FailoverSubscriptionTest {

    private PulsarClient pulsarClient;

    @Before
    public void init() throws PulsarClientException {
        pulsarClient = PulsarClient.builder()
                .serviceUrl("pulsar://localhost:6650")
                .build();
    }

    private Consumer<String> subscribe(String topicName, String subscriptionName) throws PulsarClientException {
        ConsumerBuilder<String> stringConsumerBuilder = pulsarClient.newConsumer(Schema.STRING)
                .topic(topicName)
                .subscriptionName(subscriptionName)
                .subscriptionMode(SubscriptionMode.Durable)
                .subscriptionType(SubscriptionType.Failover);

        Consumer<String> consumer = stringConsumerBuilder.subscribe();

        System.out.println("成功订阅！订阅名：" + subscriptionName + ", 主题：" + topicName);

        return consumer;
    }

    @Test
    public void testFailoverSubscription() throws PulsarClientException {

        Consumer<String> consumer = subscribe(TestProducer.TOPIC_NAME, "shared-subscription-one");

        while (true){
            Message<String> message = consumer.receive();
            System.out.println("收到消息：" + message.getValue() + ", 消息id:" + message.getMessageId() + ", key:" + message.getKey());
            consumer.acknowledge(message);
        }
    }

    @Test
    public void testFailoverSubscription2() throws PulsarClientException {

        Consumer<String> consumer = subscribe(TestProducer.TOPIC_NAME, "shared-subscription-two");

        while (true){
            Message<String> message = consumer.receive();
            System.out.println("收到消息：" + message.getValue() + ", 消息id:" + message.getMessageId() + ", key:" + message.getKey());
            consumer.acknowledge(message);
        }
    }
}
