package com.example;

import org.apache.pulsar.client.api.*;
import org.junit.Before;
import org.junit.Test;

/**
 * 测试Shared订阅类型<br>
 * <pre>
 * 测试目的：
 * 1. 确定对于相同的订阅名，能否分别同时创建两个不同订阅模式的消费者（比如：一个是Exclusive，另一个是Shared）。 结论：不能。连接的时候会抛异常：ConsumerBusyException: Subscription is of different type
 * 2. Shared订阅模式下，具有相同订阅名的两个消费者能否同时订阅同一主题？结论：能。但是生产者往该主题发送消息的时候，一条消息只会被其中一个消费者消费。
 * 3. Shared订阅模式下，具有不同订阅名的两个消费者能否同时订阅同一主题？结论：能。而且生产者往该主题发送消息的时候，一条消息会分别被两个消费者消费。
 * </pre>
 * @author linfeng
 * @date 2022/2/25 10:31
 */
public class SharedSubscriptionTest {

    private PulsarClient pulsarClient;

    @Before
    public void init() throws PulsarClientException {
        pulsarClient = PulsarClient.builder()
                .serviceUrl("pulsar://localhost:6650")
                .build();
    }

    private Consumer<String> subscribe(String topicName, String subscriptionName) throws PulsarClientException {
        ConsumerBuilder<String> stringConsumerBuilder = pulsarClient.newConsumer(Schema.STRING)
                .topic(topicName)
                .subscriptionName(subscriptionName)
                .subscriptionMode(SubscriptionMode.Durable)
                .subscriptionType(SubscriptionType.Shared);

        Consumer<String> consumer = stringConsumerBuilder.subscribe();

        System.out.println("成功订阅！订阅名：" + subscriptionName + ", 主题：" + topicName);

        return consumer;
    }

    @Test
    public void testSharedSubscription() throws PulsarClientException {

        Consumer<String> consumer = subscribe(TestProducer.TOPIC_NAME, "shared-subscription-one");

        while (true){
            Message<String> message = consumer.receive();
            System.out.println("收到消息：" + message.getValue() + ", 消息id:" + message.getMessageId());
            consumer.acknowledge(message);
        }
    }

    @Test
    public void testSharedSubscription2() throws PulsarClientException {

        Consumer<String> consumer = subscribe(TestProducer.TOPIC_NAME, "shared-subscription-one");

        while (true){
            Message<String> message = consumer.receive();
            System.out.println("收到消息：" + message.getValue() + ", 消息id:" + message.getMessageId());
            consumer.acknowledge(message);
        }
    }
}
