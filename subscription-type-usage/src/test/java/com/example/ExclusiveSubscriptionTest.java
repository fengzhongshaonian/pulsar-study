package com.example;

import org.apache.pulsar.client.api.*;
import org.junit.Before;
import org.junit.Test;

/**
 * 测试Exclusive订阅类型<br>
 * <pre>
 * 测试结果：
 * 两个使用Exclusive订阅模式的消费者：
 * 1.如果这两个消费者的订阅名相同，则它们不能同时订阅同一个主题。
 * 2.如果这两个消费者的订阅名相同，它们可以同时分别订阅不同的主题。
 * 3.如果这两个消费者的订阅名不相同，它们可以同时订阅同一个主题，而且生产者往该主题发送消息，一条消息会分别被两个消费者消费。
 * </pre>
 * @author linfeng
 * @date 2022/2/25 10:31
 */
public class ExclusiveSubscriptionTest {

    private PulsarClient pulsarClient;

    @Before
    public void init() throws PulsarClientException {
        pulsarClient = PulsarClient.builder()
                .serviceUrl("pulsar://localhost:6650")
                .build();
    }

    private Consumer<String> subscribe(String topicName, String subscriptionName) throws PulsarClientException {
        ConsumerBuilder<String> stringConsumerBuilder = pulsarClient.newConsumer(Schema.STRING)
                .topic(topicName)
                .subscriptionName(subscriptionName)
                .subscriptionMode(SubscriptionMode.Durable)
                .subscriptionType(SubscriptionType.Exclusive);

        Consumer<String> consumer = stringConsumerBuilder.subscribe();

        System.out.println("成功订阅！订阅名：" + subscriptionName + ", 主题：" + topicName);

        return consumer;
    }

    @Test
    public void testExclusiveSubscription() throws PulsarClientException {

        Consumer<String> consumer = subscribe(TestProducer.TOPIC_NAME, "exclusive-subscription-one");

        while (true){
            Message<String> message = consumer.receive();
            System.out.println("收到消息：" + message.getValue() + ", 消息id:" + message.getMessageId());
            consumer.acknowledge(message);
        }
    }

    @Test
    public void testExclusiveSubscription2() throws PulsarClientException {

        Consumer<String> consumer = subscribe(TestProducer.TOPIC_NAME, "exclusive-subscription-one");

        while (true){
            Message<String> message = consumer.receive();
            System.out.println("收到消息：" + message.getValue() + ", 消息id:" + message.getMessageId());
            consumer.acknowledge(message);
        }
    }
}
