package com.example.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author linfeng
 * @date 2022/2/28 11:08
 */
public class DateUtil {

    public static String toStr(LocalDateTime localDateTime){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String s = localDateTime.format(dateTimeFormatter);
        return s;
    }
}
