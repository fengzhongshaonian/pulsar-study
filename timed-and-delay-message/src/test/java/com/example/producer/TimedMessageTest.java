package com.example.producer;

import com.example.util.DateUtil;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.Schema;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.TimeUnit;

/**
 * 发送定时消息
 *
 * @author linfeng
 * @date 2022/2/28 9:55
 */
public class TimedMessageTest {
    public static final String TOPIC_NAME = "timed-test";

    private PulsarClient pulsarClient;

    private Producer<String> producer;

    @Before
    public void init() throws PulsarClientException {
        pulsarClient = PulsarClient.builder()
                .serviceUrl("pulsar://localhost:6650")
                .build();

        // 定时/延时 消息不可以使用 batch 模式发送，请在创建 producer 的时候把 enableBatch 参数设为 false。
        producer = pulsarClient.newProducer(Schema.STRING)
                .enableBatching(false)
                .topic(TOPIC_NAME)
                .create();
    }

    /**
     * 发送定时消息
     * @throws PulsarClientException
     */
    @Test
    public void testSendingTimedMessage() throws PulsarClientException {
        // 定时消息的时间范围为当前时间开始计算，864000秒（10天）以内的任意时刻。
        // 如10月1日12:00开始，最长可以设置到10月11日12:00。
        LocalDateTime time = LocalDateTime.now().plusMinutes(3);
        long timestamp = Date.from(time.toInstant(ZoneOffset.ofHours(8))).getTime();
        producer.newMessage()
                .deliverAt(timestamp)
                .value("这是发送于" + DateUtil.toStr(LocalDateTime.now()) + "的定时消息")
                .send();
        System.out.println("消息已发送");
    }

    /**
     * 发送延时消息
     */
    @Test
    public void testSendingDelayMessage() throws PulsarClientException {
        // 延时消息的时长取值范围为0 - 864000秒（0秒 - 10天）。
        // 如10月1日12:00开始，最长可以设置864000秒。如果设置的时间超过这个时间，则直接按864000秒计算，到时会直接投递。
        producer.newMessage()
                .deliverAfter(100, TimeUnit.SECONDS)
                .value("这是发送于" + DateUtil.toStr(LocalDateTime.now()) + "的延时消息")
                .send();
        System.out.println("消息已发送");
    }
}
